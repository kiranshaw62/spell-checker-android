package com.example.spellcheckdemo.spellchecklib.org;

import com.example.spellcheckdemo.spellchecklib.org.engine.SpellDictionary;
import com.example.spellcheckdemo.spellchecklib.org.event.SpellChecker;
import com.example.spellcheckdemo.spellchecklib.org.event.StringWordTokenizer;

public class SpellingParser {


    private final SpellChecker sc;

    public SpellingParser(SpellDictionary dict) {

        sc = new SpellChecker(dict);

        checkSpelling("e treiD something worng ni life. Well nto al do.");
    }


    public void checkSpelling(String wordToCheck){

        StringWordTokenizer swt = new StringWordTokenizer(wordToCheck);

        sc.checkSpelling(swt);
    }


}
