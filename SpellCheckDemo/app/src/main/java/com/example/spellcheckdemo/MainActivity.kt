package com.example.spellcheckdemo

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.appcompat.app.AppCompatActivity
import com.example.spellcheckdemo.spellchecklib.org.SpellingParser
import com.example.spellcheckdemo.spellchecklib.org.engine.SpellDictionaryHashMap
import kotlinx.android.synthetic.main.activity_main.*
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader


class MainActivity : AppCompatActivity() {

    private var sp: SpellingParser? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        createSpellingParser()

        suggestionsEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

//                sp?.checkSpelling(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {


            }
        })

    }

    private fun createSpellingParser() {

        sp = null

        val assetManager = assets

        val dict = SpellDictionaryHashMap()

        val american = true //Make a global property or setter.

        var `in` = assetManager.open("eng_com.dic")
        BufferedReader(InputStreamReader(`in`)).use { r -> dict.addDictionary(r) }


        val others: Array<String>
        others = if (american) {
            arrayOf(
                "color", "labeled", "center", "ize",
                "yze"
            )
        } else { // British
            arrayOf(
                "colour", "labelled", "centre",
                "ise", "yse"
            )
        }

        for (other in others) {
            `in` = assetManager.open("$other.dic")
            BufferedReader(InputStreamReader(`in`)).use { r -> dict.addDictionary(r) }
        }
        try {
            sp = SpellingParser(dict)
        } catch (ioe: IOException) {
            ioe.printStackTrace()
        }
    }

}
